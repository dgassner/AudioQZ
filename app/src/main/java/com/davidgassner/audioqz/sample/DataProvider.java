package com.davidgassner.audioqz.sample;

import com.davidgassner.audioqz.model.Cue;

import java.util.ArrayList;
import java.util.List;

public class DataProvider {

    public static List<Cue> getCueList() {
        List<Cue> cues = new ArrayList<>();

        addCue(cues, 1, "1",
                "Charleston 1",
                "charleston.wav",
                Cue.CUE_TYPE_AUDIO,
                1f);
        addCue(cues, 2, "2",
                "Explosion",
                "explosion.mp3",
                Cue.CUE_TYPE_AUDIO,
                1f);
        addCue(cues, 3, "3",
                "Piano Music",
                "pianoMusic.mp3",
                Cue.CUE_TYPE_AUDIO,
                .5f);

        return cues;
    }

    private static void addCue(List<Cue> cues,
                               int index,
                               String cueNumber,
                               String title,
                               String fileName,
                               String cueType,
                               float volume) {
        Cue cue = new Cue();
        cue.setCueIndex(index);
        cue.setCueNumber(cueNumber);
        cue.setTitle(title);
        cue.setTargetFile(fileName);
        cue.setType(cueType);
        cue.setVolume(volume);
        cue.setDuration(-1);
        cues.add(cue);
    }
}
