package com.davidgassner.audioqz.model;

public class Cue {

    public static final String CUE_TYPE_AUDIO = "AUDIO";
    public static final String CUE_TYPE_FADE = "FADE";

    private String cueId;       //primary key, random UUID set upon create
    private int cueIndex;       //to maintain order for sorting, must be unique
    private String cueNumber;   //any string, can be empty
    private String title;       //non-null string
    private String targetFile;  //file name, for Audio cues only
    private String type;        //non-null, one of the CUE_TYPE constants
    private float volume;       //set for Audio and Fade cues
    private float duration;     //derived from file for audio cues; user-set for fades

    public Cue() {
    }

    public String getCueId() {
        return cueId;
    }

    public void setCueId(String cueId) {
        this.cueId = cueId;
    }

    public int getCueIndex() {
        return cueIndex;
    }

    public void setCueIndex(int cueIndex) {
        this.cueIndex = cueIndex;
    }

    public String getCueNumber() {
        return cueNumber;
    }

    public void setCueNumber(String cueNumber) {
        this.cueNumber = cueNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getTargetFile() {
        return targetFile;
    }

    public void setTargetFile(String targetFile) {
        this.targetFile = targetFile;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public float getVolume() {
        return volume;
    }

    public void setVolume(float volume) {
        this.volume = volume;
    }

    public String getVolumeAsPercent() {
        return String.valueOf(volume * 100) + "%";
    }

    public float getDuration() {
        return duration;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "Cue{" +
                "cueIndex=" + cueIndex +
                ", targetFile='" + targetFile + '\'' +
                ", cueNumber='" + cueNumber + '\'' +
                ", title='" + title + '\'' +
                ", type='" + type + '\'' +
                ", volume='" + volume + '\'' +
                ", duration='" + duration + '\'' +
                '}';
    }
}