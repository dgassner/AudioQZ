package com.davidgassner.audioqz.util;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public class FileHelper {

    private static final String TAG = "FILEHELPER";
    public static final String APP_STORAGE_DIRECTORY =
            Environment.getExternalStorageDirectory() + "/AudioQZ/audio";

    public static String copyAssetToStorage(Context context, String fileName) {

        File persistentFile = new File(APP_STORAGE_DIRECTORY, fileName);
        if (!persistentFile.exists()) {
            try {
                InputStream inputStream = context.getAssets().open(fileName);
                int size = inputStream.available();
                byte[] buffer = new byte[size];
                int ignore = inputStream.read(buffer);
                inputStream.close();
                FileOutputStream fos = new FileOutputStream(persistentFile);
                fos.write(buffer);
                fos.close();
                Log.i(TAG, "copyAssetToStorage: Persistent file created");
            } catch (Exception e) {
                throw new AssertionError("copyAssetToStorage: " +
                        e.getMessage() + ", file: " + fileName);
            }
        }
        return persistentFile.toString();
    }

    public static void createAppStorageDirectory() {
        File storageDir = new File(FileHelper.APP_STORAGE_DIRECTORY);
        if (storageDir.exists() || storageDir.mkdirs()) {
            Log.i(TAG, "onRequestPermissionsResult: storage dir set up");
        } else {
            throw new AssertionError("onRequestPermissionsResult: storage dir not created");
        }
    }


}
