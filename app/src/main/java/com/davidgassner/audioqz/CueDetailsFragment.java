package com.davidgassner.audioqz;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.davidgassner.audioqz.database.DatabaseHelper;
import com.davidgassner.audioqz.model.Cue;

public class CueDetailsFragment extends Fragment {

    public static final String ARG_ID = "CUE_ID";
//    public static final String ARG_TITLE = "CUE_TITLE";
//    public static final String ARG_NUMBER = "CUE_NUMBER";
//    public static final String ARG_VOLUME = "CUE_VOLUME";

    private Cue mCue;

    public CueDetailsFragment() {
    }

    public static CueDetailsFragment newInstance(Cue cue) {

        Bundle args = new Bundle();
        args.putString(ARG_ID, cue.getCueId());

        CueDetailsFragment fragment = new CueDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//      Retrieve selected cue from database
        if (getArguments().containsKey(ARG_ID)) {
            DatabaseHelper dbHelper = new DatabaseHelper(getActivity());
            mCue = dbHelper.getCueById(getArguments().getString(ARG_ID));
            if (mCue != null) {
                Toast.makeText(getActivity(),
                        "Got cue " + mCue.getTitle(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cue_details, container, false);

        TextView tvNumber = (TextView) view.findViewById(R.id.tvNumber);
        TextView tvTitle = (TextView) view.findViewById(R.id.tvTitle);
        TextView tvVolume = (TextView) view.findViewById(R.id.tvVolume);

        tvNumber.setText(mCue.getCueNumber());
        tvTitle.setText(mCue.getTitle());
        tvVolume.setText(mCue.getVolumeAsPercent());

        return view;
    }

}
