package com.davidgassner.audioqz.audio;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.davidgassner.audioqz.model.Cue;

import java.util.HashMap;
import java.util.Map;

public class AudioQZService extends Service {

    private final String TAG = getClass().getSimpleName();

    // Binder given to clients and callbacks for communication from service
    private final Binder mBinder = new ServiceBinder();
    private AudioServiceListener mListener;

    // Map of currently active players
    private Map<String, Player> mActivePlayers = new HashMap<>();


    public void prepareAndPlay(Cue cue) {
        if (!cue.getType().equals(Cue.CUE_TYPE_AUDIO)) throw new AssertionError();

        String cueId = cue.getCueId();

//      Make sure we aren't playing the same cue twice
        if (mActivePlayers.containsKey(cueId)) {
            Player player = mActivePlayers.get(cueId);
            player.stop();
        }
//      Add the new player and start it up
        mActivePlayers.put(cueId, new Player(this, cue));
        mActivePlayers.get(cueId).prepareAndPlay();
    }

    public boolean isPlaying(Cue cue) {
        return mActivePlayers.containsKey(cue.getCueId());
    }

    public void setPlayerVolume(String playerKey, int volume) {
        mActivePlayers.get(playerKey).setVolume(volume);
    }

    public float getPlayerVolume(String cueId) {
        return mActivePlayers.get(cueId).getVolume();
    }

    public void removePlayer(String cueId) {
        mActivePlayers.remove(cueId);
    }

    public void stopAllPlayers() {
        for (Map.Entry<String, Player> entry : mActivePlayers.entrySet()) {
            entry.getValue().stop();
        }
    }

    //  returns START_STICKY to keep running in background
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class ServiceBinder extends Binder {
        public AudioQZService getService() {
            // Return this instance of service so clients can call public methods
            return AudioQZService.this;
        }
    }

    public interface AudioServiceListener {
        void onAudioCueComplete(String cueId);
    }

    public void setListener(AudioServiceListener listener) {
        mListener = listener;
    }

    public void notifyAudioCueComplete(String cueId) {
        mListener.onAudioCueComplete(cueId);
    }

}
