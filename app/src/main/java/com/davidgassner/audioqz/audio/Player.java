package com.davidgassner.audioqz.audio;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;

import com.davidgassner.audioqz.R;
import com.davidgassner.audioqz.model.Cue;
import com.davidgassner.audioqz.util.FileHelper;

import java.io.FileInputStream;
import java.io.IOException;

public class Player {

    private final String TAG = getClass().getSimpleName();

    private MediaPlayer mediaPlayer;
    private AudioQZService service;
    private Context context;
    private Cue cue;
    private boolean isPrepared = false;
    private float volume = 1;

    public Player(Context context, Cue cue) {
        this.context = context;
        this.cue = cue;
        this.service = (AudioQZService) context;
    }

    public boolean prepareAndPlay() {

//      TODO: Temporary - right now all audio files start off in assets
//      After adding code to import files from SD, etc. this can go away
//      Unless I retain it for long-term sample data support
//      **
        String fullFileName =
                FileHelper.copyAssetToStorage(context, cue.getTargetFile());
//      **

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                isPrepared = true;
                play();
            }
        });
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.stop();
                mp.reset();
                mp.release();
                service.notifyAudioCueComplete(cue.getCueId());
                service.removePlayer(cue.getCueId());
            }
        });

        FileInputStream stream = null;

        try {
            stream = new FileInputStream(fullFileName);
            mediaPlayer.setDataSource(stream.getFD());
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setVolume(cue.getVolume(), cue.getVolume());
            mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException ignore) {
                }
            }
        }
        return true;
    }

    public void play() {
        if (isPrepared) {
            mediaPlayer.start();
        }
    }

    //  Stop this cue and set its player object to null
    public void stop() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    //  Called from service when the user manipulates the SeekBar
    public void setVolume(float progress) {
        if (mediaPlayer == null) {
            return;
        }
        volume = progress / context.getResources().getInteger(R.integer.seek_bar_max);
        mediaPlayer.setVolume(volume, volume);
    }

    public float getVolume() {
        return volume * 100;
    }

}
