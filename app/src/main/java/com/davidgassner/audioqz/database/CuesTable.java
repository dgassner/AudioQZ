package com.davidgassner.audioqz.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.util.Log;

import com.davidgassner.audioqz.model.Cue;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

// defines cues table
public class CuesTable {

    // Cues table
    public static final String TABLE_NAME = "cues";

    // Columns
    public static final String CUE_ID = "cueId";
    public static final String CUE_NUMBER = "cueNumber";
    public static final String CUE_INDEX = "cueIndex";
    public static final String CUE_TITLE = "cueTitle";
    public static final String CUE_FILE = "cueFile";
    public static final String CUE_TYPE = "cueType";
    public static final String CUE_VOLUME = "cueVolume";
    public static final String CUE_DURATION = "cueDuration";

    private static final String TAG = "CuesTable";
    private static String[] ALL_COLUMNS =
            new String[]{CUE_ID, CUE_INDEX, CUE_NUMBER, CUE_TITLE,
                    CUE_FILE, CUE_TYPE, CUE_VOLUME, CUE_DURATION};

    public static void createTable(SQLiteDatabase db) {
        Log.i(TAG, "createTable: done");
        db.execSQL("CREATE TABLE " + TABLE_NAME + "("
                + CUE_ID + " TEXT PRIMARY KEY,"
                + CUE_INDEX + " INTEGER,"
                + CUE_TITLE + " TEXT,"
                + CUE_FILE + " TEXT,"
                + CUE_NUMBER + " TEXT,"
                + CUE_TYPE + " TEXT NOT NULL,"
                + CUE_VOLUME + " REAL,"
                + CUE_DURATION + " REAL)");
    }

    public static void dropTable(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    public static Cue getItemById(SQLiteDatabase db, String cueId) {
        Cue cue = null;
        try (Cursor cursor = db.query(TABLE_NAME, ALL_COLUMNS,
                CUE_ID + "=?",
                new String[]{String.valueOf(cueId)}, null, null, null, null)) {
            if (cursor.moveToNext()) {
                cue = getCueFromCursor(cursor);
            }
        } catch (Exception e) {
            Log.e(TAG, "getItemById: " + e.getMessage());
        }
        return cue;
    }

    @NonNull
    private static Cue getCueFromCursor(Cursor cursor) {
        Cue cue = new Cue();
        cue.setCueId(cursor.getString(cursor.getColumnIndex(CuesTable.CUE_ID)));
        cue.setCueIndex(cursor.getInt(cursor.getColumnIndex(CuesTable.CUE_INDEX)));
        cue.setCueNumber(cursor.getString(cursor.getColumnIndex(CuesTable.CUE_NUMBER)));
        cue.setTitle(cursor.getString(cursor.getColumnIndex(CuesTable.CUE_TITLE)));
        cue.setTargetFile(cursor.getString(cursor.getColumnIndex(CuesTable.CUE_FILE)));
        cue.setType(cursor.getString(cursor.getColumnIndex(CuesTable.CUE_TYPE)));
        cue.setVolume(cursor.getFloat(cursor.getColumnIndex(CuesTable.CUE_VOLUME)));
        cue.setDuration(cursor.getFloat(cursor.getColumnIndex(CuesTable.CUE_DURATION)));
        return cue;
    }

    public static List<Cue> getAllCues(SQLiteDatabase db) {
        List<Cue> cueList = new ArrayList<>();

        try (Cursor cursor = db.query(TABLE_NAME, ALL_COLUMNS,
                null, null, null, null, CUE_INDEX, null)) {
            while (cursor.moveToNext()) {
                Cue cue = getCueFromCursor(cursor);
                cueList.add(cue);
            }
        } catch (Exception e) {
            Log.e(TAG, "getItemById: " + e.getMessage());
        }
        return cueList;
    }

    /**
     * Add a new Cue
     * The CUE ID is a random UUID rather than an auto-incrementing int
     * The ID is used to manage players from the client, and also to keep track
     * of them in the service
     */
    public static void createCue(SQLiteDatabase db, Cue item) {
        ContentValues values = new ContentValues();
        values.put(CUE_ID, UUID.randomUUID().toString());
        values.put(CUE_INDEX, item.getCueIndex());
        values.put(CUE_TITLE, item.getTitle());
        values.put(CUE_FILE, item.getTargetFile());
        values.put(CUE_NUMBER, item.getCueNumber());
        values.put(CUE_TYPE, item.getType());
        values.put(CUE_VOLUME, item.getVolume());
        values.put(CUE_DURATION, item.getDuration());

        // add the row
        db.insert(TABLE_NAME, null, values);
    }

    public static void updateTitleAndNumber(SQLiteDatabase db, Cue item) {
        ContentValues values = new ContentValues();
        values.put(CUE_TITLE, item.getTitle());
        values.put(CUE_NUMBER, item.getCueNumber());

        // update the row
        int result = db.update(TABLE_NAME, values,
                CUE_ID + "=?",
                new String[]{item.getCueId()});
        if (result != 1) {
            Log.e(TAG, "Expected result of db operation to be 1, got " + result);
        }
    }

}
