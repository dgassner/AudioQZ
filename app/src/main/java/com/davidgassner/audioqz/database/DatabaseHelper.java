package com.davidgassner.audioqz.database;

import android.content.Context;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.davidgassner.audioqz.sample.DataProvider;
import com.davidgassner.audioqz.model.Cue;

import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    private final String TAG = getClass().getSimpleName();

    public static final String DATABASE_NAME = "audioqz";
    public static final int DATABASE_VERSION = 1;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        seedDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        ProjectsTable.createTable(db);
        CuesTable.createTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        ProjectsTable.dropTable(db);
        CuesTable.dropTable(db);

        onCreate(db);
    }

    public Cue getCueById(String id) {
        SQLiteDatabase db = getReadableDatabase();
        return CuesTable.getItemById(db, id);
    }

    public List<Cue> getAllCues() {
        SQLiteDatabase db = getReadableDatabase();
        return CuesTable.getAllCues(db);
    }

    public void updateCue(Cue cue) {
        SQLiteDatabase db = getReadableDatabase();
        CuesTable.updateTitleAndNumber(db, cue);
    }

    public void seedDatabase() {

        Log.i(TAG, "seedDatabase: seeding");

        SQLiteDatabase db = getReadableDatabase();

        long numCues = DatabaseUtils.queryNumEntries(db, CuesTable.TABLE_NAME);

        if (numCues == 0) {
            List<Cue> dummyCues = DataProvider.getCueList();
            for (Cue cue : dummyCues) {
                CuesTable.createCue(db, cue);
            }
        }

    }

}
