package com.davidgassner.audioqz.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import com.davidgassner.audioqz.R;
import com.davidgassner.audioqz.database.DatabaseHelper;
import com.davidgassner.audioqz.model.Cue;

public class CueEditDialog extends DialogFragment {

    private static final String TAG = "CueEditDialog";
    private Cue mCue;
    private CueEditFragmentListener mListener;

    private static final String ARG_ID = "CUE_ID";
    private EditText etNumber, etTitle, etVolume;
    private SeekBar seekBar;

    public CueEditDialog() {
    }

    public static CueEditDialog newInstance(Cue cue) {

        Bundle args = new Bundle();
        args.putString(ARG_ID, cue.getCueId());

        CueEditDialog fragment = new CueEditDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ID)) {
            DatabaseHelper dbHelper = new DatabaseHelper(getActivity());
            mCue = dbHelper.getCueById(getArguments().getString(ARG_ID));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_cue_edit, container, false);

        etNumber = (EditText) view.findViewById(R.id.etCueNumber);
        etNumber.setImeOptions(EditorInfo.IME_ACTION_NEXT);

        etTitle = (EditText) view.findViewById(R.id.etTitle);
        etTitle.setImeOptions(EditorInfo.IME_ACTION_NEXT);

        etVolume = (EditText) view.findViewById(R.id.etVolume);
        etVolume.setImeOptions(EditorInfo.IME_ACTION_DONE);

        etNumber.setText(mCue.getCueNumber());
        etTitle.setText(mCue.getTitle());

        float volume = mCue.getVolume() * 100;
        etVolume.setText(String.valueOf((int)volume));

        seekBar = (SeekBar) view.findViewById(R.id.seekBar);
        seekBar.setProgress((int) volume);

//      Hide soft keyboard when user presses Done button
        etVolume.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) ||
                        (actionId == EditorInfo.IME_ACTION_DONE)) {
                    InputMethodManager imm = (InputMethodManager) getActivity()
                            .getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
                return false;
            }
        });

//      Button handlers
        Button btnSave = (Button) view.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveChanges();
            }
        });

        Button btnCancel = (Button) view.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return view;
    }

    private void saveChanges() {
        mCue.setCueNumber(etNumber.getText().toString());
        mCue.setTitle(etTitle.getText().toString());
        mListener.onCueEditComplete(mCue);
        dismiss();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof CueEditFragmentListener) {
            mListener = (CueEditFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement CueEditFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface CueEditFragmentListener {
        void onCueEditComplete(Cue cue);
    }

}
