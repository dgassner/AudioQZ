package com.davidgassner.audioqz;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.davidgassner.audioqz.audio.AudioQZService;
import com.davidgassner.audioqz.database.DatabaseHelper;
import com.davidgassner.audioqz.dialogs.CueEditDialog;
import com.davidgassner.audioqz.model.Cue;
import com.davidgassner.audioqz.util.FileHelper;

import java.util.List;

public class MainActivity extends AppCompatActivity
        implements CueEditDialog.CueEditFragmentListener,
        AudioQZService.AudioServiceListener {

    private final String TAG = getClass().getSimpleName();
    private static final int PERMISSION_REQUEST_WRITE = 1001;

    //    private CueListFragment mListFragment;
    private TextView mCueTitleText, mVolumeText;
    private SeekBar mSeekBar;
    private ViewGroup mPlayerControls;

    private List<Cue> mCueList;
    private int mCurrentCueIndex;
    private CueListAdapter mListAdapter;

    private Cue mSelectedCue;

    private AudioQZService qzService;
    private DatabaseHelper dbHelper;
    private ListView listView;

    private boolean mTablet;

    //  Service management code, including connection object and binding/unbinding
    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder binder) {
            AudioQZService.ServiceBinder serviceBinder =
                    (AudioQZService.ServiceBinder) binder;
            qzService = serviceBinder.getService();
            qzService.setListener(MainActivity.this);
        }

        public void onServiceDisconnected(ComponentName className) {
            if (qzService != null) {
                qzService.setListener(null);
                qzService = null;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//      Detect FrameLayout in tablet version of screen design
        mTablet = (findViewById(R.id.cue_details_container) != null);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initWidgetIds();
        initListenersAndState();
        initCuesDisplay();
        initAudioPlayerService();
        checkPermissions();

    }

    //  Set up display of audio cues
//    TODO: right now there's just one cue set, eventually user will be able to choose a "show"
    private void initCuesDisplay() {

        listView = (ListView) findViewById(R.id.cue_list);
        if (listView == null) throw new AssertionError();
        listView.setSelector(R.drawable.list_background_colors);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectCue(position);
            }
        });

        dbHelper = new DatabaseHelper(this);
        mCueList = dbHelper.getAllCues();
        mListAdapter = new CueListAdapter(this, mCueList);
        listView.setAdapter(mListAdapter);

        // Select the first cue upon startup
        if (mCueList.size() > 0) {
            listView.setItemChecked(0, true);
            selectCue(0);
        }
    }

    //  Called upon startup, when user selects cue from list, or upon running a cue
    private void selectCue(int position) {
        mSelectedCue = mCueList.get(position);
        mCurrentCueIndex = position;
        if (qzService != null && qzService.isPlaying(mSelectedCue)) {
            mPlayerControls.setVisibility(View.VISIBLE);
            int volume = (int) qzService.getPlayerVolume(mSelectedCue.getCueId());
            mSeekBar.setProgress(volume);
        } else {
            mPlayerControls.setVisibility(View.INVISIBLE);
        }

        if (mTablet) {
            CueDetailsFragment fragment = CueDetailsFragment.newInstance(mSelectedCue);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.cue_details_container, fragment)
                    .commit();
        } else {
            mCueTitleText.setText(mSelectedCue.getTitle());
        }
    }

    //  Set up id's for widgets
    private void initWidgetIds() {
        mPlayerControls = (ViewGroup) findViewById(R.id.playerControls);
        mCueTitleText = (TextView) findViewById(R.id.tvCueTitle);
        mVolumeText = (TextView) findViewById(R.id.volumePercentText);
        mSeekBar = (SeekBar) findViewById(R.id.seekBar);
    }

    //  Start the service that manages audio players
    private void initAudioPlayerService() {
        Intent serviceIntent = new Intent(this, AudioQZService.class);
        startService(serviceIntent);
    }

    //  Set up widget listeners and states
    //  Button click events are defined in layout file
    private void initListenersAndState() {

        mPlayerControls.setVisibility(View.INVISIBLE);
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                qzService.setPlayerVolume(mSelectedCue.getCueId(), progress);
                mVolumeText.setText(String.format("%d%%", progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

    }

    //  Set up options menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case R.id.action_setup_storage:
                checkPermissions();
                return true;
            case R.id.action_stop_all:
                stopAllCues();
                return true;
            case R.id.new_cue_audio:
                addNewCue(Cue.CUE_TYPE_AUDIO);
                return true;
            case R.id.action_edit_cue:
                editCue();
                return true;
            case R.id.new_cue_fade:
                addNewCue(Cue.CUE_TYPE_FADE);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addNewCue(String cueType) {
        Toast.makeText(MainActivity.this, "Creating new cue " + cueType, Toast.LENGTH_SHORT).show();
    }

    //  Run currently selected cue
    public void runCue(View view) {
        if (mSelectedCue == null) throw new AssertionError();
        try {
            qzService.prepareAndPlay(mSelectedCue);
            mSeekBar.setProgress((int) (mSeekBar.getMax() * mSelectedCue.getVolume()));
            mVolumeText.setText(mSelectedCue.getVolumeAsPercent());
            mPlayerControls.setVisibility(View.VISIBLE);

            if (mCurrentCueIndex < (mCueList.size() - 1)) {
                listView.setItemChecked(mCurrentCueIndex + 1, true);
                selectCue(mCurrentCueIndex + 1);
            }

        } catch (Exception e) {
            Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void stopAllCues() {
        qzService.stopAllPlayers();
        mPlayerControls.setVisibility(View.INVISIBLE);
    }

    // Bind to the service
    @Override
    protected void onResume() {
        super.onResume();
        Intent serviceIntent = new Intent(this, AudioQZService.class);
        bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE);
    }

    // Unbind from service
    @Override
    protected void onPause() {
        super.onPause();
        unbindService(mConnection);
    }

    private void checkPermissions() {

        int permissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            FileHelper.createAppStorageDirectory();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_WRITE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_WRITE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    FileHelper.createAppStorageDirectory();
                } else {
                    Toast.makeText(MainActivity.this,
                            R.string.must_grant,
                            Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public void editCue() {
        DialogFragment dialog = CueEditDialog.newInstance(mSelectedCue);
        dialog.setCancelable(false);
        dialog.show(getSupportFragmentManager(), "CueEditDialog");
    }

    public void actionDeleteClickHandler(MenuItem item) {
        Toast.makeText(MainActivity.this, "Delete selected", Toast.LENGTH_SHORT).show();

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        Toast.makeText(MainActivity.this, "Yes", Toast.LENGTH_SHORT).show();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        Toast.makeText(MainActivity.this, "No", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.confirm_delete)
                .setPositiveButton(android.R.string.yes, dialogClickListener)
                .setNegativeButton(android.R.string.no, dialogClickListener)
                .show();

    }

    @Override
    public void onCueEditComplete(Cue newCue) {

//      Update in database
        dbHelper.updateCue(newCue);

//      Update in ListView
        mSelectedCue.setTitle(newCue.getTitle());
        mSelectedCue.setCueNumber(newCue.getCueNumber());
        mListAdapter.notifyDataSetChanged();

//      Update in details view
        selectCue(mCurrentCueIndex);
    }

    @Override
    public void onAudioCueComplete(String cueId) {
        Toast.makeText(MainActivity.this, "Cue complete", Toast.LENGTH_SHORT).show();
        if (mSelectedCue.getCueId().equals(cueId)) {
            mPlayerControls.setVisibility(View.INVISIBLE);
        }
    }
}
